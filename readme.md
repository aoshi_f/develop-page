# Simplate

Create static webpage simply.
Change `index.html` and `main.scss` to make own page...

## Prerequisites

- yarn
- node
- npm

## Usage

```
$ yarn
$ yarn watch # watching file: index.html, main.scss
```

## Todo

- Support JavaScript
- Import style/js from node_modules
